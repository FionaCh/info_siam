//Bibliothèques
#include "principal.h"

#define LIG 11
#define COL 16

//Ici implémenter les méthodes relatives au plateau de jeu

//MODELE
//Remplissage de la map
void remplir_plateau (std::map <std::string, std::string> &  plateau)
{/*
    for (std::string i = "A"; i < "F"; i[0]++)
    {
        for (std::string j = "1"; j < "6"; j[0]++)
        {
            plateau[i+j] = "A";
        }
    }
   */

    std::vector< std::vector<int> > plateau (LIG, std::vector<int> (COL));


    std::ifstream fic("tab.txt");

    if (fic) // test si le fichier est bien ouvert
    {
        std::string line;
        while (std::getline(fic, line)) // lecture ligne par ligne
        {
            //BLEHHHHHHHHHHHHH
            plateau[i][j] = atoi(line.c_str());
            std::cout << plateau[i][j];

            if(j>=COL)
            {
                i++;
                j=0;
            }
        }
        fic.close(); // fermeture du flux
    }
    else // en cas d'erreur...
    {
        std::cout << "Cannot read file tab.txt" << std::endl;
    }
}


//VUE

void afficher_plateau (std::map <std::string, std::string> &  plateau)
{
    //Ressources
    int compteur = 0;

    for (const auto elem : plateau)
    {
        std::cout << " " << elem.first;
        compteur ++;
        if(compteur == 5)
        {
            std::cout << std::endl;
            compteur = 0;
        }
    }
}
