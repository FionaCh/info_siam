#ifndef PLATEAU_H_INCLUDED
#define PLATEAU_H_INCLUDED

//Ici d�clarer les prototypes relatifs au plateau de jeu (remplissage de la map, etc...)

void remplir_plateau (std::map <std::string, std::string> & plateau);
void afficher_plateau (std::map <std::string, std::string> & plateau);

#endif // PLATEAU_H_INCLUDED
