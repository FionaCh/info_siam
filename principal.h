#ifndef PRINCIPAL_H_INCLUDED
#define PRINCIPAL_H_INCLUDED

//Ici, ajouter au fur et � mesure les biblioth�ques pour n'avoir qu'une biblioth�que � passer dans les .cpp
//Organisation des biblioth�ques � changer pour la conception mod�le-vu-contr�leur : https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur

//Biblioth�ques
//STANDARD
#include <iostream>
#include <string>
#include <map>
#include <list>
#include <fstream>
#include <vector>

//PERSONNALISEES
#include "pions.h"
#include "animal.h"
#include "plateau.h"
#include "joueur.h"


#endif // PRINCIPAL_H_INCLUDED
