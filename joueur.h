#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED

class Joueur
{
private :
    std::string m_pseudo;
    std::list <Animal> m_pions;
    int m_montagnes;

public:
    Joueur(std::string _pseudo, char _type);
    ~Joueur();
};


#endif // JOUEUR_H_INCLUDED
