#ifndef ANIMAL_H_INCLUDED
#define ANIMAL_H_INCLUDED

//Biblioth�que
#include "pions.h"

//Class fille, d�riv�e de la classe Pion
class Animal : public Pion
{
private :
    char m_orientation; //Orientation de l'animal

public :
    //Constucteur surcharg�
    Animal(char _type);
    //Destructeur
    ~Animal();

    //Accesseur
    //Getter
    char getOrientation();
    //Setter
    void setOrientation(char _orientation);

    //M�thode
    void pousser(); //Pousser un autre pion : animal ou montagne
    void se_tourner(); //Changer son orientation
    void sortir(); //Sortir du plateau

};

#endif // ANIMAL_H_INCLUDED
