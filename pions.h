#ifndef PIONS_H_INCLUDED
#define PIONS_H_INCLUDED

//Ici d�clarer les classes relatives aux �lements du plateau de jeu : classe m�re pion + classes filles animaux et montagnes

//Classe m�re
class Pion
{
protected : //D�claration des attributs
    char m_type; //Char qui correspond � la lettre qui apparait sur le plateau
    int m_resistanceCotes; // R�sistance de c�t�s du pion
    int m_resistanceFace; // R�sistance de face du pion

public :
    //Constructeur surcharg�
    Pion(char _type, int _resistanceCotes, int _resistanceFace);
    //Destructeur
    virtual ~Pion();

    //M�thode
    virtual void deplacer(std::map <std::string, std::string> & plateau);
};

#endif // PIONS_H_INCLUDED
